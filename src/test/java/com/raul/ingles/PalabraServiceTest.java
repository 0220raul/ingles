package com.raul.ingles;

import com.raul.ingles.business.dto.CategoriaDto;
import com.raul.ingles.business.dto.PalabraDto;
import com.raul.ingles.business.service.CategoriaService;
import com.raul.ingles.business.service.PalabraService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PalabraServiceTest {

    @Autowired
    PalabraService palabraService;

    @Autowired
    CategoriaService categoriaService;

    @Test
    @Order(1)
    public void test() {

    }

    /*@Test
    @Order(2)
    public void save() {
        List<CategoriaDto> categorias = categoriaService.findAll();
        final String[] espanol = new String[1];
        final String[] ingles = new String[1];
        categorias.forEach(categoriaDto -> {
            espanol[0] = new RandomString().generateString();
            ingles[0] = new RandomString().generateString();
            PalabraDto palabraDto = PalabraDto.builder()
                    .espanol(espanol[0])
                    .ingles(ingles[0])
                    .categoriaDto(categoriaDto).build();
            palabraDto = palabraService.save(palabraDto);
            Assertions.assertNotNull(palabraDto.getIdPalabra());
            Assertions.assertEquals(espanol[0], palabraDto.getEspanol());
            Assertions.assertEquals(ingles[0], palabraDto.getIngles());
        });
    }*/

    /*@Test
    @Order(3)
    public void findAll() {
        Assertions.assertTrue(palabraService.findAll().size() > 0, "La lista es igual o menor a 0");;
    }*/
}
