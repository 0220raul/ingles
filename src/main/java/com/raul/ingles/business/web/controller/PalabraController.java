package com.raul.ingles.business.web.controller;

import com.raul.ingles.business.dto.PalabraDto;
import com.raul.ingles.business.service.PalabraService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController()
@RequestMapping("palabras")
@CrossOrigin(origins = "*")
public class PalabraController {

    private final Logger logger = Logger.getLogger(this.getClass().getName());
    final PalabraService palabraService;

    public PalabraController(PalabraService palabraService) {
        this.palabraService = palabraService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PalabraDto> findById(@PathVariable Long id) {
        return new ResponseEntity<>(palabraService.findById(id), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<PalabraDto>> palabras() {
        logger.info("ListPalabras");
        return new ResponseEntity<>(palabraService.findAll(), HttpStatus.OK);
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PalabraDto> save(@RequestBody PalabraDto palabraDto) {
        logger.info(new StringBuilder().append("SavePalabra: ").append(palabraDto).toString());
        return new ResponseEntity<>(palabraService.save(palabraDto), HttpStatus.OK);
    }

}
