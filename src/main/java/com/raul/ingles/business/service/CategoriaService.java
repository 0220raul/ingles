package com.raul.ingles.business.service;

import com.raul.ingles.business.dto.CategoriaDto;

import java.util.List;

public interface CategoriaService {
    CategoriaDto save(CategoriaDto categoria);
    Boolean delete(CategoriaDto categoriaDto);
    List<CategoriaDto> findAll();
    CategoriaDto findById(Long id);
    CategoriaDto findByNombreCategoria(String categoria);
}
