package com.raul.ingles.business.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "categoria")
@Getter @Setter @NoArgsConstructor
public class Categoria {

    @Id
    @Column(name = "id_Categoria")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idCategoria;
    @Column(name = "nombre_Categoria")
    private String nombreCategoria;
    @OneToMany(mappedBy = "categoria", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Palabra> palabras;
}
