package com.raul.ingles.business.dto;

import lombok.*;

import java.util.Date;

@NoArgsConstructor @Getter @Setter @AllArgsConstructor @Builder
public class PalabraDto {

    private Long idPalabra;
    private String ingles;
    private String espanol;
    private String significado;
    private String descripcion;
    private String urlImagen;
    private Date fecha;
    private CategoriaDto categoriaDto;
}
