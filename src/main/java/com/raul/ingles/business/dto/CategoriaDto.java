package com.raul.ingles.business.dto;

import lombok.*;

@Getter @Setter @RequiredArgsConstructor @NoArgsConstructor
public class CategoriaDto {
    private Long idCategoria;
    @NonNull
    private String nombreCategoria;
}
