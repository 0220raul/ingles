package com.raul.ingles;

import com.raul.ingles.business.dto.CategoriaDto;
import com.raul.ingles.business.service.CategoriaService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CategoriaServiceTest {

    @Autowired
    CategoriaService service;

    private final String[] categorias = { "Acronyms", "Actions with the hands", "Adjective forms", "Adverbs of frequency",
            "Adverbs of time", "American and British English", "Animal body parts", "Animal sounds", "Animal young",
            "Animals", "Animals", "At the airport", "At the beach", "At the hotel",
            "At the hotel (Technical English)", "At the restaurant", "At the restaurant",
            "At the supermarket", "Bathroom", "Birds", "Body actions", "Chess", "Christmas", "Christmas Cookies",
            "Cinema", "Clothes", "Clothes", "Collective nouns", "Colours", "Communication",
            "Containers", "Daily activities", "Dairy products", "Describing people", "Drinks", "Easter",
            "Education", "Environment", "False Friends or Cognates", "Father's Day Poems",
            "Fish, crustaceans and molluscs", "Flavours", "Flowers", "Food", "Football",
            "Free time activities", "Fruit and vegetables", "Fruit and vegetables",
            "Gardening", "Geography", "Halloween", "Health and illness", "Herbs and spices", "Homophones",
            "House", "In the classroom", "Kitchen utensils", "Law", "Letter writing",
            "List of Irregular Verbs", "List of Verbs (Advanced)", "List of Verbs (Basic)", "Materials",
            "Means of transport", "Measures", "Military rank", "Money", "Mother's Day Poems", "Music",
            "Musical instruments", "Nationalities", "No Money", "Numbers", "Office items", "Office items",
            "Olympic Games", "On a picnic", "Ordinal Numbers", "Over 30 Forms of Money", "Parts of a bicycle",
            "Parts of a car", "Parts of a car", "Parts of a plane", "Parts of a ship", "Parts of a train",
            "Parts of the body", "Parts of the body", "Parts of the house", "Personal information",
            "Personality - Negative aspect", "Personality - Positive aspect", "Photography", "Places and buildings",
            "Places and buildings", "Politics", "Professions and occupations",
            "Professions and occupations", "Recipe ingredients", "Religion", "Slang words for 'toilet'",
            "Slang words used in songs", "Sports", "Sports", "Telephoning", "Tennis", "Thanksgiving",
            "The Family", "The Media", "Theatre", "Tools", "Tourism", "Trees", "TV and cartoon characters",
            "Uncountable nouns made countable", "Ways of saying 'strange'", "Ways of saying 'Very Good'",
            "Ways of saying 'You're crazy'", "Weather", "Weather",
            "Words and expressions related to 'age'", "Words and expressions related to 'name'",
            "Words used only in plural", "Work and employment" };

    @Test
    public void save() {
        /* String categoria;
        for (int i = 0; i < 10; i++) {
            categoria = new RandomString().generateString();
            Assertions.assertEquals(categoria, service.save(new CategoriaDto(categoria)).getNombreCategoria());
        } */
        for (String categoria : categorias) {
            Assertions.assertEquals(categoria, service.save(new CategoriaDto(categoria)).getNombreCategoria());
        }

    }

}
