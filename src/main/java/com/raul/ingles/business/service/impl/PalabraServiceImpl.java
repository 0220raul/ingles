package com.raul.ingles.business.service.impl;

import com.raul.ingles.business.dto.CategoriaDto;
import com.raul.ingles.business.dto.PalabraDto;
import com.raul.ingles.business.model.Palabra;
import com.raul.ingles.business.repository.PalabraRepository;
import com.raul.ingles.business.service.PalabraService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;

@Service
@Transactional
public class PalabraServiceImpl implements PalabraService {

    private final PalabraRepository palabraRepository;
    private final ModelMapper mapper;

    public PalabraServiceImpl(PalabraRepository palabraRepository, ModelMapper mapper) {
        this.palabraRepository = palabraRepository;
        this.mapper = mapper;
    }

    @Override
    public PalabraDto save(PalabraDto palabraDto) {
        return palabraToPalabraDto(palabraRepository.save(palabraDtoToPalabra(palabraDto)));
    }

    @Override
    public Boolean delete(PalabraDto palabraDto) {
        //TODO verificar que el total no cambie al hacer la segunda ejecución
        long total = palabraRepository.count();
        palabraRepository.delete(palabraDtoToPalabra(palabraDto));
        return total < palabraRepository.count();
    }

    @Override
    public List<PalabraDto> findAll() {
        Type listType = new TypeToken<List<PalabraDto>>(){}.getType();
        List<Palabra> palabras = palabraRepository.findAll();
        List<PalabraDto> palabraDtos = mapper.map(palabras, listType);
        for (int i = 0; i < palabraDtos.size(); i++) {
            palabraDtos.get(i).setCategoriaDto(mapper.map(palabras.get(i).getCategoria(), CategoriaDto.class));
        }
        return palabraDtos;
    }

    @Override
    public PalabraDto findById(Long id) {
        /*Palabra palabra = palabraRepository.findById(id).orElse(null);
        PalabraDto palabraDto = palabraToPalabraDto(palabra);
        palabraDto.setCategoriaDto(mapper.map(palabra != null ? palabra.getCategoria() : null, CategoriaDto.class));*/
        return palabraToPalabraDto(palabraRepository.findById(id).orElse(null));
    }

    @Override
    public PalabraDto findByEspanolOrIngles(String espanol, String ingles) {
        return palabraToPalabraDto(palabraRepository.findByEspanolOrIngles(espanol, ingles));
    }

    private PalabraDto palabraToPalabraDto(Palabra palabra) {
        return mapper.map(palabra, PalabraDto.class);
    }

    private Palabra palabraDtoToPalabra(PalabraDto palabraDto) {
        return mapper.map(palabraDto, Palabra.class);
    }
}
