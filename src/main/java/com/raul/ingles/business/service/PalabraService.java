package com.raul.ingles.business.service;

import com.raul.ingles.business.dto.PalabraDto;

import java.util.List;

public interface PalabraService {
    PalabraDto save(PalabraDto palabraDto);
    Boolean delete(PalabraDto palabraDto);
    List<PalabraDto> findAll();
    PalabraDto findById(Long id);
    PalabraDto findByEspanolOrIngles(String espanol, String ingles);
}
