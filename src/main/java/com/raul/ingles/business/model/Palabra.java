package com.raul.ingles.business.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "palabra")
@Getter @Setter @NoArgsConstructor
public class Palabra {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_Palabra")
    private Long idPalabra;
    @Column(unique = true)
    private String ingles;
    @Column(unique = true)
    private String espanol;
    @Column
    private String significado;
    @Column
    private String descripcion;
    @Column(name = "url_Imagen")
    private String urlImagen;
    @Column(name = "fecha")
    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date fecha;

    @ManyToOne(fetch = FetchType.LAZY)
    Categoria categoria;

    @PrePersist
    private void onPrePersist(){
        fecha = new Date();
    }
}
