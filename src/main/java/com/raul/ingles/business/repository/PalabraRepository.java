package com.raul.ingles.business.repository;

import com.raul.ingles.business.model.Palabra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PalabraRepository extends JpaRepository<Palabra, Long> {
    Palabra findByEspanolOrIngles(String espanol, String ingles);
}
