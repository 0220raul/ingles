package com.raul.ingles.business.repository;

import com.raul.ingles.business.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {
    Categoria findByNombreCategoria(String nombreCategoria);
}
