package com.raul.ingles.business.service.impl;

import com.raul.ingles.business.dto.CategoriaDto;
import com.raul.ingles.business.model.Categoria;
import com.raul.ingles.business.repository.CategoriaRepository;
import com.raul.ingles.business.service.CategoriaService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;

@Service
@Transactional
public class CategoriaServiceImpl implements CategoriaService {

    private final CategoriaRepository repository;
    private final ModelMapper mapper;

    public CategoriaServiceImpl(CategoriaRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public CategoriaDto save(CategoriaDto categoriaDto) {
        return categoriaToCategoriaDto(repository.save(categoriaDtoToCategoria(categoriaDto)));
    }

    @Override
    public Boolean delete(CategoriaDto categoriaDto) {
        //TODO verificar que el total no cambie al hacer la segunda ejecución
        long total = repository.count();
        repository.delete(categoriaDtoToCategoria(categoriaDto));
        return total > repository.count();
    }

    @Override
    public List<CategoriaDto> findAll() {
        Type listType = new TypeToken<List<CategoriaDto>>(){}.getType();
        return mapper.map(repository.findAll(), listType);
    }

    @Override
    public CategoriaDto findById(Long id) {
        return categoriaToCategoriaDto(repository.findById(id).orElse(null));
    }

    @Override
    public CategoriaDto findByNombreCategoria(String categoria) {
        return categoriaToCategoriaDto(repository.findByNombreCategoria(categoria));
    }

    private Categoria categoriaDtoToCategoria(CategoriaDto categoriaDto){
        return mapper.map(categoriaDto, Categoria.class);
    }

    private CategoriaDto categoriaToCategoriaDto(Categoria categoria){
        return mapper.map(categoria, CategoriaDto.class);
    }
}
